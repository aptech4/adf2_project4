package thread;

public class FirstThread extends Thread {
    @Override
    public void run() {
        super.run();
        int index = 0;
        while (index < 10) {
            System.out.println("thread.FirstThread and current index is " + index);
            index ++;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
