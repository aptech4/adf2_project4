package thread;

public class SecondThread extends Thread {
    @Override
    public void run() {
        super.run();
        int index = 0;
        while (index < 20) {
            System.out.println("thread.SecondThread and current index is " + index);
            index ++;

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
