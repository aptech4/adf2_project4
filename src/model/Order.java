package model;

public class Order {
    private String productCode;
    private String productName;
    private double retailPrice;
    private int quantity;
    private double totalMoney;

    public Order() {
    }

    public Order(String productCode, String productName, double retailPrice, int quantity, double totalMoney) {
        this.productCode = productCode;
        this.productName = productName;
        this.retailPrice = retailPrice;
        this.quantity = quantity;
        this.totalMoney = totalMoney;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public double getRetailPrice() {
        return retailPrice;
    }

    public void setRetailPrice(double retailPrice) {
        this.retailPrice = retailPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(double totalMoney) {
        this.totalMoney = totalMoney;
    }
}
