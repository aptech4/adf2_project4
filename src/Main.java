import exception.TriangleEdgesException;
import model.Order;
import model.Product;
import thread.FirstThread;
import thread.SecondThread;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class Main {
    static String FILE_PRODUCT = "products.txt";
    static String FILE_ORDER = "orders.txt";

    public static void main(String[] args) {
        question02();
    }

    static void question02() {
        while (true) {
            Scanner scanner = new Scanner(System.in);
            System.out.print("\n1. Tạo đơn bán hàng.\n" +
                    "2. Quản lý đơn bán hàng.\n3. Xem báo cáo bán hàng.\n4. Thêm mới sản phẩm.\n" +
                    "5. Danh sách sản phẩm.\n6. Thoát.\nNhập tuỳ chọn: ");

            int option = 0;
            try {
                option = scanner.nextInt();
            } catch (InputMismatchException ex) {
                System.out.println("Tuỳ chọn bạn nhập không hợp lệ!");
                continue;
            }

            switch (option) {
                case 1:
                    createNewOrder();
                    break;
                case 2:
                    ArrayList<Order> orders = readOrderFromFile();
                    System.out.println(orders.size() == 0 ? "Chưa có đơn hàng nào!" : "Danh sách đơn hàng:");

                    for (Order order : orders) {
                        String format = String.format(Locale.getDefault(), "%s | %s | %f | %d | %f",
                                order.getProductCode(), order.getProductName(), order.getRetailPrice(),
                                order.getQuantity(), order.getTotalMoney());
                        System.out.println(format);
                    }
                    break;
                case 3:
                    showReport();
                    break;
                case 4:
                    Product newProduct = createNewProduct();
                    saveProductToFile(newProduct);
                    break;
                case 5:
                    ArrayList<Product> products = readProductFromFile();
                    System.out.println(products.size() == 0 ? "Chưa có sản phẩm nào!" : "Danh sách sản phẩm:");

                    for (Product product : products) {
                        String format = String.format(Locale.getDefault(), "%s | %s | %f | %f | %d",
                                product.getProductCode(), product.getProductName(), product.getPriceRetail(),
                                product.getImportPrice(), product.getQuantityInStock());
                        System.out.println(format);
                    }
                    break;
                case 6:
                    return;
                default:
                    break;
            }
        }
    }

    static void showReport() {
        ArrayList<Order> orders = readOrderFromFile();

        //Tổng số lượng sản phẩm đã bán
        int totalQuantity = 0;
        //Tổng tiền hàng đã bán
        double totalMoney = 0;
        for (Order order : orders) {
            totalQuantity += order.getQuantity();
            totalMoney += order.getTotalMoney();
        }

        double totalImportPrice = 0;
        for (Order order : orders) {
            Product product = findProductByCode(order.getProductCode());
            if (product != null) {
                totalImportPrice += order.getQuantity() * product.getImportPrice();
            }
        }

        double totalProfit = totalMoney - totalImportPrice;

        System.out.println("Báo cáo bán hàng:");
        System.out.println("Tổng số lượng sản phẩm đã bán: " + totalQuantity);
        System.out.println("Tổng tiền bán hàng: " + totalMoney);
        System.out.println("Tổng lãi: " + totalProfit);
    }

    static void createNewOrder() {
        while (true) {
            Scanner scanner = new Scanner(System.in);
            System.out.print("Nhập mã sản phẩm bán: ");
            String productCode = scanner.nextLine();

            Product product = findProductByCode(productCode);
            if (product == null) {
                System.out.println("Không tìm thấy sản phẩm với mã tương ứng. Vui lòng nhập lại!");
                continue;
            }

            System.out.print("Nhập số lượng bán: ");
            int quantity = 0;
            try {
                quantity = scanner.nextInt();
            } catch (InputMismatchException ex) {
                System.out.println("Số lượng nhập không hợp lệ. Vui lòng nhập lại!");
                continue;
            }

            if (quantity <= 0) {
                System.out.println("Số lượng bán phải lớn hơn 0!");
                continue;
            }

            double totalMoney = quantity * product.getPriceRetail();
            Order order = new Order(product.getProductCode(), product.getProductName(),
                    product.getPriceRetail(), quantity, totalMoney);

            saveOrderToFile(order);
            break;
        }
    }

    static ArrayList<Order> readOrderFromFile() {
        ArrayList<Order> orderList = new ArrayList<>();
        File file = new File(FILE_ORDER);
        if (!file.exists()) {
            return orderList;
        }

        try {
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                String lineData = scanner.nextLine();
                String[] elements = lineData.split("\\|");

                if (elements.length != 5) {
                    continue;
                }
                String productCode = elements[0].trim();
                String productName = elements[1].trim();
                double retailPrice = Double.parseDouble(elements[2].trim());
                int quantity = Integer.parseInt(elements[3].trim());
                double totalMoney = Double.parseDouble(elements[4].trim());

                orderList.add(new Order(productCode, productName, retailPrice, quantity, totalMoney));
            }
        } catch (FileNotFoundException e) {
            System.out.println("Không tìm thấy file!");
        }

        return orderList;
    }

    static void saveOrderToFile(Order order) {
        File file = new File(FILE_ORDER);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.out.println("Không thể khởi tạo file");
                return;
            }
        }

        try {
            FileWriter fileWriter = new FileWriter(file, true);

            String data = String.format(Locale.getDefault(), "%s | %s | %f | %d | %f\n",
                    order.getProductCode(), order.getProductName(), order.getRetailPrice(),
                    order.getQuantity(), order.getTotalMoney());

            fileWriter.write(data);

            fileWriter.close();
            System.out.println("Đã lưu đơn hàng!");
        } catch (IOException e) {
            System.out.println("Không thể lưu dữ liệu vào file!");
        }
    }

    static Product findProductByCode(String productCode) {
        Product product = null;
        ArrayList<Product> products = readProductFromFile();
        for (Product prod : products) {
            if (prod.getProductCode().equalsIgnoreCase(productCode)) {
                product = prod;
                break;
            }
        }
        return product;
    }

    static ArrayList<Product> readProductFromFile() {
        ArrayList<Product> productList = new ArrayList<>();
        File file = new File(FILE_PRODUCT);
        if (!file.exists()) {
            return productList;
        }

        try {
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                String lineData = scanner.nextLine();
                String[] elements = lineData.split("\\|");

                if (elements.length != 5) {
                    continue;
                }
                String productCode = elements[0].trim();
                String productName = elements[1].trim();
                double retailPrice = Double.parseDouble(elements[2].trim());
                double importPrice = Double.parseDouble(elements[3].trim());
                int quantityInStock = Integer.parseInt(elements[4].trim());

                productList.add(new Product(productCode, productName, retailPrice,
                        importPrice, quantityInStock));
            }
        } catch (FileNotFoundException e) {
            System.out.println("Không tìm thấy file!");
        }

        return productList;
    }

    static void saveProductToFile(Product product) {
        File file = new File(FILE_PRODUCT);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.out.println("Không thể khởi tạo file.");
                return;
            }
        }

        try {
            FileWriter fileWriter = new FileWriter(file, true);

            String data = String.format(Locale.getDefault(), "%s | %s | %f | %f | %d\n",
                    product.getProductCode(), product.getProductName(), product.getPriceRetail(),
                    product.getImportPrice(), product.getQuantityInStock());

            fileWriter.write(data);

            fileWriter.close();
            System.out.println("Đã lưu sản phẩm!");
        } catch (IOException e) {
            System.out.println("Không thể lưu dữ liệu vào file!");
        }
    }

    static Product createNewProduct() {
        String productCode = "";
        String productName = "";
        double retailPrice = 0;
        double importPrice = 0;
        int quantityInStock = 0;
        while (true) {
            Scanner scanner = new Scanner(System.in);

            System.out.print("Nhập mã sản phẩm: ");
            productCode = scanner.nextLine();

            System.out.print("Nhập tên sản phẩm: ");
            productName = scanner.nextLine();

            System.out.print("Giá bán lẻ sản phẩm: ");
            try {
                retailPrice = scanner.nextDouble();
            } catch (InputMismatchException ex) {
                System.out.println("Giá bán lẻ không hợp lệ.");
                continue;
            }

            System.out.print("Giá nhập sản phẩm: ");
            try {
                importPrice = scanner.nextDouble();
            } catch (InputMismatchException ex) {
                System.out.println("Giá nhập không hợp lệ.");
                continue;
            }

            System.out.print("Số lượng tồn kho: ");
            try {
                quantityInStock = scanner.nextInt();
            } catch (InputMismatchException ex) {
                System.out.println("Số lượng tồn kho không hợp lệ.");
                continue;
            }
            break;
        }
        return new Product(productCode, productName, retailPrice, importPrice, quantityInStock);
    }

    static void question03() {
        FirstThread firstThread = new FirstThread();
        SecondThread secondThread = new SecondThread();

        firstThread.start();
        secondThread.start();
    }

    static void question01() {
        while (true) {
            Scanner scanner = new Scanner(System.in);
            float a, b, c;
            try {
                System.out.print("Nhập cạnh a = ");
                a = scanner.nextFloat();

                System.out.print("Nhập cạnh b = ");
                b = scanner.nextFloat();

                System.out.print("Nhập cạnh c = ");
                c = scanner.nextFloat();
            } catch (InputMismatchException ex) {
                System.out.println("Giá trị bạn vừa nhập không hợp lệ. Vui lòng nhập lại!");
                continue;
            }

            try {
                checkTriangleEdgesValid(a, b, c);
                break;
            } catch (TriangleEdgesException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    static void checkTriangleEdgesValid(float a, float b, float c) throws TriangleEdgesException {
        if (a + b > c && b + c > a && a + c > b) {
            System.out.println("Tam giác bạn vừa nhập là hợp lệ!");
        } else {
            throw new TriangleEdgesException();
        }
    }
}